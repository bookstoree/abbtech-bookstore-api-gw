package org.abbtech.practice.solid.inversion;

public class EmailMessageSender implements MessageSender {
    @Override
    public void sendMessage() {
        System.out.println("Send Email message");

    }
}
