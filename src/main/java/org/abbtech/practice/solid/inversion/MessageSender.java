package org.abbtech.practice.solid.inversion;

public interface MessageSender {
    void sendMessage();
}
