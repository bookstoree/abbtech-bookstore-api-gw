package org.abbtech.practice.solid.liskov;

public class BirdUtil {

    public void fly(Bird bird) {
        bird.fly();
    }
}
