package org.abbtech.practice.solid.openclose;

public class MariaDbConnection implements ConnectionService {
    @Override
    public void getConnection() {
        System.out.println("MariaDB");
    }
}
